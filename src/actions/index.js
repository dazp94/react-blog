import jsonPlaceholder from "../apis/jsonPlaceholder";
import _ from "lodash";
export const fetchPostsAndUsers = () => async (dispatch, getState) => {
  await dispatch(fetchPosts());
  console.log(getState());

  // const userIds = _.uniq(_.map(getState().posts, "userId"));
  // userIds.forEach(id => fetchUser(id));

  _.chain(getState().posts)
    .map("userId")
    .uniq()
    .forEach(id => fetchUser(id));
};

export const fetchPosts = () => async dispatch => {
  const response = await jsonPlaceholder.get("/posts");
  dispatch({ type: "FETCH_POSTS", payload: response.data });
};

export const fetchUser = id => async dispatch => {
  // console.log("I'm here with id: ", id);
  const response = await jsonPlaceholder.get(`/users/${id}`);
  dispatch({ type: "FETCH_USER", payload: response.data });
};
